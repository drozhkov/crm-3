/* global document, window, localStorage */

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import store from './store';
import './css/reset.css';
import './css/base.css';
import App from './components/App/App';
import LoginContainer from './containers/LoginContainer';
import Employee from './components/Employee/Employee';
import { DIT_DATA } from './constants';

function renderApp() {
  render(
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/login" component={LoginContainer} />
          <Route path="/id_:id" component={Employee} />
        </Switch>
      </Router>
    </Provider>,
    document.getElementById('app')
  );
}

renderApp();

if (module.hot) {
  module.hot.accept('./components/App/App', renderApp);
}

window.addEventListener('beforeunload', () => {
  localStorage.removeItem(DIT_DATA);
});
