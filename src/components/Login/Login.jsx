import React from 'react';
import { Formik, Form, Field } from 'formik';
import style from './Login.css';
import Input from '../Input/Input';
import Button from '../Button/Button';
import NavContainer from '../../containers/NavContainer';
import validateLoginForm from '../../utils/validateLoginForm';

const Login = ({ signIn }) => (
  <div className={style.login}>
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={values => validateLoginForm(values)}
      onSubmit={(values, actions) => {
        signIn(values, actions);
      }}
    >
      {({ isSubmitting }) => (
        <Form className={style.login__form}>
          <div className={style.login__wrap}>
            <Field type="email" name="email" placeholder="Email" component={Input} />
          </div>
          <div className={style.login__wrap}>
            <Field type="password" name="password" placeholder="Password" component={Input} />
          </div>
          <div className={style.login__wrap}>
            <Button type="submit">Login</Button>
          </div>
        </Form>
      )}
    </Formik>
    <NavContainer route="/">Back</NavContainer>
  </div>
);

export default Login;
