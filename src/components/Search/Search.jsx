import React from 'react';
import PropTypes from 'prop-types';
import style from './Search.css';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.searchTerm
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  onChangeHandler(event) {
    this.setState({ value: event.target.value }, () => {
      if (this.state.value.length > 2) {
        this.props.setSearchTerm(this.state.value);
      } else {
        this.props.setSearchTerm('');
      }
    });
  }

  render() {
    return (
      <div className={style.search}>
        <form action="/" className={style.search__form}>
          <input
            type="text"
            name="search"
            className={style.search__input}
            value={this.state.value}
            placeholder="Search..."
            onChange={this.onChangeHandler}
          />
        </form>
        <div className={style.search__dropdown} />
      </div>
    );
  }
}

Search.defaultProps = {
  searchTerm: ''
};

Search.propTypes = {
  searchTerm: PropTypes.string,
  setSearchTerm: PropTypes.func.isRequired
};

export default Search;
